Spécification en SDL d'une machine de café tel que :

* chaque café coûte 1 €.
* la machine à café a la capacité de rendre la monnaie sur les pièces de 2 € et les billets de 5, 10, 20 et 50 €.
* le nombre de cafés par jour que la machine peut délivrer est limité et sa valeur est N = 1000.
* il existe 5 clients spécifiés dans le système.

Génération de trois traces MSC.